import java.util.ArrayList;

import java.util.concurrent.ThreadLocalRandom;

public class Cave {
	public int etage;
	public ArrayList<Niveau> niveaux_cave = new ArrayList<Niveau>(); // liste des niveaux contenus dans cette cave

	public Cave(int vEtage) {
		this.etage = vEtage;
	}

	public void genererNiveaux() { // g�nere un nombre de niveau correspondant � l'�tage de la cave, et en fait une liste 
		int k = 0;
		switch (this.etage) {
		case 1:
			k = ThreadLocalRandom.current().nextInt(9, 12 + 1);
			break;
		case 2:
			k = ThreadLocalRandom.current().nextInt(6, 9 + 1);
			break;
		case 3:
			k = ThreadLocalRandom.current().nextInt(3, 6 + 1);
			break;
		default:
			System.out.println("nop");
			break;
		}

		
		for (int i = 0; i < k; i++) {
			niveaux_cave.add(new Niveau(100 * etage + i, etage));
		}
		ajouterNiveaux(); //ajoute les niveaux de cette cave dans la liste contenant tous les niveaux
	}

	private void ajouterNiveaux() { // ajoute les niveaux de cette cave dans la liste globale contenant tous les niveaux
		for (Niveau k : niveaux_cave) {
			Partie.liste_niveaux.add(k);
		}

	}

	public void voirNiveaux() { //affiche en console les niveaux de cette cave
		System.out.print("[");
		for (Niveau k : this.niveaux_cave)
			System.out.print("Niveau" + k.numero + " ");
		System.out.println("]");
	}
}