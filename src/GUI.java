import java.awt.Font;

import edu.princeton.cs.introcs.StdDraw;

public class GUI {

	public static void fenetre() {
		StdDraw.setCanvasSize(1200, 900);
		StdDraw.clear(StdDraw.BOOK_LIGHT_BLUE);
		}

	public static void info() {
		clear();
		caves();
		scores();
		oxygen();
		phase();
		joueurs();
		StdDraw.enableDoubleBuffering();
		StdDraw.show(50);
	}

	public static void oxygen() {
		StdDraw.setPenColor(StdDraw.BOOK_BLUE);
		Font currentFont = StdDraw.getFont();
		float size = 22;
		currentFont = currentFont.deriveFont(size);
		StdDraw.setFont(currentFont);
		StdDraw.text(0.5, 0.92, "Oxygen : " + Oxygen.bar_oxygen);
		if(Oxygen.bar_oxygen >10)
			StdDraw.setPenColor(StdDraw.BOOK_LIGHT_BLUE);
		else
			StdDraw.setPenColor(StdDraw.BOOK_RED);
		
		if (Oxygen.bar_oxygen>=0)
			StdDraw.filledRectangle(0.5, 0.90, (double) Oxygen.bar_oxygen / 400, 0.01);
		StdDraw.setFont();
	}

	public static void scores() {
		StdDraw.setPenColor(StdDraw.ORANGE);
		StdDraw.filledRectangle(0.5, 0.94, 0.495, 0.06);
		StdDraw.setPenColor();
		Font currentFont = StdDraw.getFont();
		float size = 20;
		currentFont = currentFont.deriveFont(size);
		StdDraw.setFont(currentFont);
		StdDraw.text(0.1, 0.95, "Scores : ");
		StdDraw.setPenColor();
		for (int i = 0; i < Partie.liste_joueurs.size(); i++) {
			int nom = Integer.parseInt(Partie.liste_joueurs.get(i).nom); // String to int . methode utile pour avoir P1
																			// et P2 dans le bon ordre � l'affichage
			StdDraw.textLeft((0.1 * (nom + 1)), 0.95, "P" + nom + " - " + Partie.liste_joueurs.get(i).score); 
		}
		StdDraw.setFont();
	}

	public static void caves() {
		for (int i = 0; i < Partie.liste_niveaux.size(); i++) {
			int numero = Partie.liste_niveaux.get(i).numero;
			if (numero >= 100 && numero < 200) {
				StdDraw.setPenColor(StdDraw.ORANGE);
//				StdDraw.picture(0.5, 0.83 - i * 0.03, "murJaune.png", 0.98, 0.03001  );
			}else if (numero >= 200 && numero < 300) {
				 StdDraw.setPenColor(StdDraw.PRINCETON_ORANGE);
//				StdDraw.picture(0.5, 0.83 - i * 0.03, "murOrange.png", 0.98, 0.03001  );
			}else if (numero >= 300 && numero < 400) {
				StdDraw.setPenColor(StdDraw.BOOK_RED);
//				StdDraw.picture(0.5, 0.83 - i * 0.03, "murRouge.png", 0.98, 0.03001 );
			}else
				System.out.println("erreur: "+ numero);
			
			StdDraw.rectangle(0.5, 0.83 - i * 0.03, 0.40, 0.015);
			StdDraw.setPenColor(StdDraw.ORANGE);
			StdDraw.text(0.93-(i%2)*0.03, 0.83 - i * 0.03,""+Partie.liste_niveaux.get(i).liste_coffres.size());//+"/"+ Partie.liste_niveaux.get(i).numero );//affiche le nombre de coffre � ce niveau
			StdDraw.picture(0.90+(i%2)*0.03, 0.83 - i * 0.03,"coffre.png");
		}
	}

	public static void joueurs() {
		for (int i=0;i<Partie.liste_joueurs.size();i++) {
			int pos = Partie.liste_joueurs.get(i).position;
			int nom = Integer.parseInt(Partie.liste_joueurs.get(i).nom);
			if (nom == 1) {
				StdDraw.picture(0.3*nom,0.83-(pos)*0.03 , "J1.png", 0.17, 0.07);	
			} else {
				StdDraw.setPenColor(StdDraw.RED);
				StdDraw.picture(0.3*nom,0.83-(pos)*0.03 , "J2.png", 0.17, 0.07);	
			}
		}
	}

	public static void phase() {
		StdDraw.setPenColor();
		Font currentFont = StdDraw.getFont();
		float size = 20;
		currentFont = currentFont.deriveFont(size);
		StdDraw.setFont(currentFont);
		StdDraw.text(0.8, 0.95, "Phase " + Partie.phase + "/3");
		StdDraw.setFont();

	}

	public static void fin() {
		StdDraw.disableDoubleBuffering();
		String joueurGagnant;
		
		StdDraw.clear(StdDraw.BLACK);
		StdDraw.setPenColor(StdDraw.WHITE);
		StdDraw.rectangle(0.5, 0.5, 0.495, 0.06);
		
		Font currentFont = StdDraw.getFont(); //Gestion de la police d'�criture
		float size = 30;
		currentFont = currentFont.deriveFont(size);
		StdDraw.setFont(currentFont);
		StdDraw.setPenColor(StdDraw.WHITE);
		StdDraw.text(0.5, 0.5, "----------------- GAME OVER ! -----------------");
		StdDraw.text(0.5, 0.3, " press \"DOWN\" to play again !");
//		StdDraw.setFont();
				
		if (Partie.liste_joueurs.get(1).score>Partie.liste_joueurs.get(0).score) { 
			joueurGagnant = Partie.liste_joueurs.get(1).nom;
			StdDraw.text(0.5, 0.45, "Winner : J"+ joueurGagnant );
		} else if (Partie.liste_joueurs.get(1).score < Partie.liste_joueurs.get(0).score) {
			joueurGagnant = Partie.liste_joueurs.get(0).nom;
			StdDraw.text(0.5, 0.45, "Winner : J"+ joueurGagnant );
		} else
			StdDraw.text(0.5, 0.45, "It's a tie !");
	}

	public static void multiplayer() {
		StdDraw.clear(StdDraw.BLACK);
		StdDraw.setPenColor(StdDraw.WHITE);
		StdDraw.rectangle(0.5, 0.5, 0.495, 0.06);
		Font currentFont = StdDraw.getFont();
		float size = 30;
		currentFont = currentFont.deriveFont(size);
		StdDraw.setFont(currentFont);
		StdDraw.setPenColor(StdDraw.WHITE);
		StdDraw.text(0.5, 0.5, " Press \"UP\" to play against AI, \"DOWN\" for multiplayer ! ");
		StdDraw.setFont();
	}

	public static void clear() {
		StdDraw.picture(0.5,0.5, "fond.jpg");	
	}

	public static void pasDeCoffre() {
		// TODO Auto-generated method stub
		StdDraw.disableDoubleBuffering();
		StdDraw.setPenColor(StdDraw.WHITE);
		Font currentFont = StdDraw.getFont();
		float size = 25;
		currentFont = currentFont.deriveFont(size);
		StdDraw.setFont(currentFont);
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.text(0.5, 0.97, "No chest here !");
		StdDraw.setFont();
		StdDraw.enableDoubleBuffering();
	}
}