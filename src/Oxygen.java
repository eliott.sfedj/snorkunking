public class Oxygen {
	public static int bar_oxygen = 2 * Partie.liste_niveaux.size();

	public static int getOxygen() {// permet de retourner l'oxygen restant
		return Oxygen.bar_oxygen;
	}

	public static void majOxygen(int valeur) {// permet de décrémenter la barre d'oxygen d'une certaine valeur
		Oxygen.bar_oxygen -= valeur;
	}

	public static void newOxygen() {// permet de réinitialiser la barre d'oxygen
		Oxygen.bar_oxygen = 2 * Partie.liste_niveaux.size();
	}

}