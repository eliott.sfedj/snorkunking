import java.util.ArrayList;

public class Niveau extends Cave {
	public int etage;
	public int numero;
	public ArrayList<Coffre> liste_coffres = new ArrayList<Coffre>(); // liste des coffres contenus dans ce niveau
	
	
	public Niveau(int numero, int etage) {
		super(etage);
		this.etage = super.etage;
		this.numero = numero;
		this.liste_coffres.add(new Coffre(this.etage, 1));
	}

	public void voirCoffres() { // affiche en console la liste des coffres de ce niveau
		System.out.print("[");
		for (Coffre k : this.liste_coffres)
			System.out.print("Coffre" + k.ID + " ");
		System.out.println("]");
	}

	public void ajouterCoffre(Coffre coffre) {// ajoute un coffre � ce niveau
		this.liste_coffres.add(coffre);
	}

	public static void supprNiveau() {
		for (int k=0;k<Partie.liste_niveaux.size();k++) {
			Niveau niveauk=Partie.liste_niveaux.get(k);
			if (niveauk.liste_coffres.isEmpty()) {
				Partie.liste_niveaux.remove(niveauk);
			}
		}
		
	}


}