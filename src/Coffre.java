import java.util.concurrent.ThreadLocalRandom;

public class Coffre extends Cave {
	public int ID;
	public int etage;

	public Coffre(int etage, int ID) {
		super(etage);
		this.etage = super.etage;
		this.ID = ID;
	}

	public int ouvrir() { // permet d'ouvrir ce coffre, et r�vele un nombre de tr�sor en fonction de l'�tage d'origine.
		int k = 0;
		switch (this.etage) {
		case 1:
			k = ThreadLocalRandom.current().nextInt(1, 3 + 1);
			break;
		case 2:
			k = ThreadLocalRandom.current().nextInt(5, 8 + 1);
			break;
		case 3:
			k = ThreadLocalRandom.current().nextInt(10, 12 + 1);
			break;
		default:
			System.out.println("il n'y a que 3 �tages !");
			break;
		}
		return k;

	}

}
