import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import edu.princeton.cs.introcs.StdDraw;

public class Joueur {
	public boolean multi = false;
	public int score = 0;
	public ArrayList<Coffre> liste_porte = new ArrayList<Coffre>(); // liste contenant les coffres port�s par le joueur
	public int position = 0;
	public String nom;
	// public Niveau niveau_actuel = Partie.liste_niveaux.get(position);

	public Joueur(String nom) {
		this.nom = nom;
		Partie.liste_joueurs.add(this);

	}

	public int getPosition() { // renvoie la position absolue du joueur (part � 0, puis est incr�ment�e de 1 ou
								// -1 � chaque tour)
		return this.position;
	}

	public int getNiveauActuel() { // renvoie la position relative du joueur
		// (on prend le niveau d'indice "position absolue" dans la liste contenant tous
		// les niveaux g�n�r�s.
		return Partie.liste_niveaux.get(position).numero;

	}

	public void deplacement(int mouvement) { // permet d'incr�menter la position absolue du joueur de 1 ou -1
		if (multi == true) {
			if (Partie.liste_niveaux.get(getPosition()).liste_coffres.size() != 0) {
				this.prendreCoffre();
			} else if (this.liste_porte.size() > 3 && this.getPosition() != 0 ) {
				mouvement = -1;
			} else if (this.liste_porte.size() > 3 && this.getPosition() == 0) {
				this.deposerCoffre();
			} else
				mouvement = 1;
		}

		if (this.position == 0) { // si le joueur est en surface, il ne peut que descendre.
			this.deposerCoffre();
			this.position += 1;
			// System.out.println("Descendre");
			Oxygen.majOxygen(this.liste_porte.size() + 1); // permet de d�cr�menter sur la barre d'oxygen
		} else if (this.position == Partie.liste_niveaux.size() - 1) {// s'il est tout en bas, il ne peut que monter
			this.position -= 1;
			// System.out.println("Monter");
			Oxygen.majOxygen(this.liste_porte.size()+1);
		} else {
			switch (mouvement) {// sinon, il est libre de ses mouvements
			case 1:
				this.position -= 1;
				Oxygen.majOxygen(this.liste_porte.size()+1);
				// System.out.println("Monter");
				break;
			case -1:
				this.position += 1;
				// System.out.println("Descendre");
				Oxygen.majOxygen(this.liste_porte.size()+1);

				break;
			default:
				Oxygen.majOxygen(1);
				break;
			
			}
		}
	}

	public void prendreCoffre() { // permet de remove le dernier �l�ment de la liste de coffre d'un niveau
		// pour l'ajouter dans la liste des coffres port�s par le joueur
		Niveau niveau_actuel = Partie.liste_niveaux.get(position);
		if (niveau_actuel.liste_coffres.size() != 0) {
			this.liste_porte.add(niveau_actuel.liste_coffres.get(niveau_actuel.liste_coffres.size() - 1));
			niveau_actuel.liste_coffres.remove(niveau_actuel.liste_coffres.size() - 1);
			Oxygen.majOxygen(1);
		} else {
			System.out.println("Pas de coffre � ce niveau !"); // A IMPLANTER
			GUI.pasDeCoffre();
		}
	}

	public void deposerCoffre() { // permet de remove tous les coffres port�s par le joueur pour les ouvrir et
									// indenter le score.
		if (this.position == 0 && this.liste_porte.size() != 0) {
			for (Coffre coffre : this.liste_porte) {
				this.score += coffre.ouvrir();
			}
			this.liste_porte.clear();
		} else
			System.out.println("Impossible !");
	}

	public void tuerJoueur() { // permet de tuer un joueur s'il n'y a plus d'oxygen et s'il n'est pas � la
								// surface.
		if (this.position != 0) {
			for (Coffre coffre : this.liste_porte) { // remove tous les coffres port�s pour les ajouter � la liste des
														// coffres du dernier niveau.
				Partie.liste_niveaux.get(Partie.liste_niveaux.size() - 1).ajouterCoffre(coffre);
			}
			this.liste_porte.clear();
			System.out.println(this.nom + " : Vous �tes mort !");
			this.position = 0; // renvoie le joueur tu� en surface.
		}
		this.position = 0;
	}
}
