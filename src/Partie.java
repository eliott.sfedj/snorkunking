import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import edu.princeton.cs.introcs.StdDraw;
import javax.sound.sampled.AudioSystem;
import javax.swing.JOptionPane;

import sun.audio.*;


public class Partie {
	public static ArrayList<Niveau> liste_niveaux = new ArrayList<Niveau>();// liste contenant tous les niveau g�n�r�s
	public static ArrayList<Joueur> liste_joueurs = new ArrayList<Joueur>();// liste contenant tous les joueurs g�n�r�s
	public static int phase = 0; // num�ro de la phase de jeu
	public static boolean replay = false;

	public static void main(String[] args) {
		PlaySound();
		try {
			do {
				StdDraw.clear();
				liste_niveaux=new ArrayList<Niveau>();
				liste_joueurs= new ArrayList<Joueur>();
				phase =1;
			
				GUI.fenetre(); // Cr�ation de la fen�tre Std Draw

				Cave cave1 = new Cave(1); // g�n�ration des caves
				Cave cave2 = new Cave(2);
				Cave cave3 = new Cave(3);
				cave1.genererNiveaux(); // g�neration des niveaux
				cave2.genererNiveaux();
				cave3.genererNiveaux();
				tousNiveaux();

				Joueur joueur1 = new Joueur("1"); // g�n�ration des deux joueurs ; A MODIFIER (generation dans un ordre al�atoire pour alterner le premier joueur
				Joueur joueur2 = new Joueur("2");

				multi();// fonction de selection du multi
				GUI.clear();
				GUI.info(); // affichage de l'interface de jeu

				do { // boucle de gestion de la phase (1,2,3)
						// changer_phase(); // change de phase de jeu
					tousNiveaux();

					do { // boucle de la r�serve d'O2

						for (Joueur joueur : liste_joueurs) { // parcours la liste des joueurs
							boolean control = false;
							while (!control) {

								if (joueur.multi == true) {
									joueur.deplacement(1);
									control = true;
								}

								if ((StdDraw.isKeyPressed(KeyEvent.VK_UP))) { // gestion des touches du clavier
									joueur.deplacement(1);
									control = true;
								} else if ((StdDraw.isKeyPressed(KeyEvent.VK_DOWN))) {
									joueur.deplacement(-1);
									control = true;
								} else if ((StdDraw.isKeyPressed(KeyEvent.VK_SPACE))) {
									System.out.println("prendre/d�poser un coffre");
									control = true;
									if (joueur.position == 0)
										joueur.deposerCoffre();
									else
										joueur.prendreCoffre();
								}
							}
							control = false;
							StdDraw.pause(300);// pause, pour eviter de jouer le tour de l'autre

							if (oxygenVide())
								break;
							GUI.info();
							tousNiveaux();
						}
					} while (!oxygenVide());
					changer_phase(); // change de phase de jeu
				} while (phase <= 3);
				System.out.println("1");
				fin();
				System.out.println("2");
			} while (replay = true);
			StdDraw.clear(StdDraw.BLACK);
	} catch (Exception e) {
		 JOptionPane.showMessageDialog(null, e);
	}
}
	private static void multi() { // gestion du mode multijoueur
		GUI.multiplayer(); // affiche l'interface de d�but (pour choisir son mode)
		StdDraw.pause(1000);
		boolean controlEntree = false;
		while (!controlEntree) {
			if ((StdDraw.isKeyPressed(KeyEvent.VK_UP))) {
				liste_joueurs.get(1).multi = true;
				controlEntree = true;
			} else if ((StdDraw.isKeyPressed(KeyEvent.VK_DOWN))) {
				liste_joueurs.get(1).multi = false;
				controlEntree = true;
				GUI.clear();
			}
		}
	}

	public static void tousNiveaux() { // affiche la liste des numeros des niveaux g�n�r�s.
		System.out.print("[");
		for (Niveau k : liste_niveaux)
			System.out.print("Niveau" + k.numero + " ");
		System.out.println("]");
	}

	public static void tousJoueurs() { // affiche la liste des joueurs g�n�r�s.
		System.out.print("[");
		for (Joueur k : liste_joueurs)
			System.out.print("joueur" + k.nom + " ");
		System.out.println("]");
	}

	public static void changer_phase() { // r�initialise la barre d'oxygen, et "tue" les joueurs" (fais couler leurs
											// tr�sors)
		phase += 1;
		Oxygen.newOxygen();// r�init. la barre d'O2
		liste_joueurs.get(0).tuerJoueur();
		liste_joueurs.get(1).tuerJoueur();
		Niveau.supprNiveau(); // supprime les niveaux ne contenant pas de coffre

	}

	public static boolean oxygenVide() { // revoie true si la r�serve d'O2 est vide, false sinon
		boolean o2 = (Oxygen.getOxygen() <= 0) ? true : false;
		return o2;
	}

	public static void fin() { // fonction appel�e � la fin du jeu. Affiche l'interface de fin (ecran + vainqueur)
		boolean control=false;
		GUI.fin();
		StdDraw.pause(1500);
		while (!control) {
			if ((StdDraw.isKeyPressed(KeyEvent.VK_UP))) {
				replay = true;
				control = true;
			} else if ((StdDraw.isKeyPressed(KeyEvent.VK_DOWN))) {
				replay = false;
				StdDraw.clear();
				control=true;
			}
		}
	}
	
		public static void PlaySound() {
			InputStream in;
		    try {
		        in = new FileInputStream(new File("music.wav"));
		        AudioStream audios = new AudioStream(in);
		        AudioPlayer.player.start(audios);
		    } catch (Exception e) {
		        JOptionPane.showMessageDialog(null, e);

		    }
		}
	}
